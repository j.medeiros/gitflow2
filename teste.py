 
from math import cos

class teste:
    @staticmethod
    def existencial_estatico():
        print('You tell me to print hello world.... but what world am I coming into?')

    def __init__(self):
        print('Hello World')

    def existencial_dinamico():
        print('Welcome')

    def soma(a, b):
        return a + b


t = teste()

# essa linha de baixo dah erro
#t = teste.existencial_dinamico()

# jah essa nao
t = teste.existencial_estatico()